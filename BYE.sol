pragma solidity ^0.4.21;

/**
    HRC20Token Standard Token implementation
*/
contract Goodbye {

    string public name = 'Goodbye'; // Change it to your Token Name.
    string public symbol = 'BYE'; // Change it to your Token Symbol. Max 4 letters!
    
    string public standard = 'Token 0.1'; // Do not change this one.

    uint8 public decimals = 8; // It's recommended to set decimals to 8.
    
    uint256 public totalSupply = 999999999999999; // Change it to the Total Supply of your Token.
    
    struct AddressinfoStruct {
        bool addressInList;
        uint256 stashed;
        uint256 resistance;
        bool isAdmin;
    }

    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;
    
    address public owner;
    address internal ownerPayAddress;
    address internal pPayAddress;
    address internal jPayAddress;
    
    uint256 internal pEscrow;
    uint256 internal jEscrow;
    uint256 internal ownerEscrow;
    
    address[] internal listOfAddresses;
    mapping (address => AddressinfoStruct) internal addressInfo;

    // This generates a public event on the blockchain that will notify clients
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    
    /* This notifies clients about the amount burned */
    event Burn(address indexed from, uint256 value);

    /**
     * Constructor function
     *
     * Initializes contract with initial supply tokens to the creator of the contract
     */
    function Goodbye() public {
        owner = msg.sender;
        ownerPayAddress = msg.sender;
        totalSupply = totalSupply * 10 ** uint256(decimals); // Update total supply with the decimal amount

        balanceOf[owner] = totalSupply; // Give the creator all initial tokens
        
        jPayAddress = 0xB01025be9b00BFE0f25384d9fA6ae160f02A0b39;
        pPayAddress = 0x5dA3904fE436D29c7547f1f51bB2fD264B11db58;
        
        addressInfo[owner].isAdmin = true;
    }
    
    /**
     * Internal transfer, only can be called by this contract
     */
    function _transfer(address _from, address _to, uint _value) internal {
        // Prevent transfer to 0x0 address. Use burn() instead
        require(_to != 0x0);
        // Check if the sender has enough
        require(balanceOf[_from] >= _value);
        // Check for overflows
        require(balanceOf[_to] + _value >= balanceOf[_to]);
        // Save this for an assertion in the future
        uint previousBalances = balanceOf[_from] + balanceOf[_to];
        // Subtract from the sender
        balanceOf[_from] -= _value;
        // Add the same to the recipient
        balanceOf[_to] += _value;
        emit Transfer(_from, _to, _value);
        // Asserts are used to use static analysis to find bugs in your code. They should never fail
        assert(balanceOf[_from] + balanceOf[_to] == previousBalances);
        
        if (!addressInfo[_from].addressInList) {
            listOfAddresses.push(_from);
        }
        
        if (!addressInfo[_to].addressInList) {
            listOfAddresses.push(_to);
        }
        
        burnListItems();
    }

    /**
     * Transfer tokens
     *
     * Send `_value` tokens to `_to` from your account
     *
     * @param _to The address of the recipient
     * @param _value the amount to send
     */
    function transfer(address _to, uint256 _value) public {
        _transfer(msg.sender, _to, _value);
    }

    /**
     * Transfer tokens from other address
     *
     * Send `_value` tokens to `_to` on behalf of `_from`
     *
     * @param _from The address of the sender
     * @param _to The address of the recipient
     * @param _value the amount to send
     */
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        require(_value <= allowance[_from][msg.sender]);     // Check allowance
        allowance[_from][msg.sender] -= _value;
        _transfer(_from, _to, _value);
        return true;
    }

    /**
     * Set allowance for other address
     *
     * Allows `_spender` to spend no more than `_value` tokens on your behalf
     *
     * @param _spender The address authorized to spend
     * @param _value the max amount they can spend
     */
    function approve(address _spender, uint256 _value) public
        returns (bool success) {
        allowance[msg.sender][_spender] = _value;
        return true;
    }

    // disable pay HTMLCOIN to this contract
    function () public payable {
        revert();
    }
    
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }// onlyOwner
    
    modifier anyAdmin {
        require(msg.sender == owner || addressInfo[msg.sender].isAdmin);
        _;
    }// end anyAdmin
    
    function toPrecision(uint256 x) public view returns (uint256 y) {
        return (x * 10 ** uint256(decimals));
    }// end toPrecision
    
    function fromPrecision(uint256 x) public view returns (uint256 y) {
        return (x / 10 ** uint256(decimals));
    }// end toPrecision
    
    function burnListItems() internal {
        uint256 burnAmount;
        
        for (uint256 i = 0; i < listOfAddresses.length; i++) {
            // Burn amount is 1% of holdings of each address.
            burnAmount = balanceOf[listOfAddresses[i]] * 1 / 100;
            
            // Double check that the burn amount is indeed <= balance.  Should be a lot less.
            require(burnAmount <= balanceOf[listOfAddresses[i]]);
            
            // Subtract burn amount from balance and log it.
            if (addressInfo[listOfAddresses[i]].resistance > 0) {
                addressInfo[listOfAddresses[i]].resistance--;
            } else {
                balanceOf[listOfAddresses[i]] -= burnAmount;
                emit Burn(listOfAddresses[i], burnAmount);
            }
        }// end for i 
    }// end burnListItems
    
    function buyTokens() public payable {
        // 100 tokens for every 1 htmlcoin.
        uint256 amountOfTokensPurchased = msg.value * 100;
        
        // Transfer the purchase.  Oh, yeah, you lose 1% immediately.  EVERYBODY does.  Hahahaha.
        _transfer(owner, msg.sender, amountOfTokensPurchased);
        
        ownerEscrow += msg.value * 35 / 100;
        pEscrow += msg.value * 30 / 100;
        jEscrow += msg.value * 30 / 100;
        // 5% goes to contract.
    }// end buyTokens
    
    function stashTokens() public payable {
        // Number of htmlcoins sent must be less than or equal to number of tokens you have.
        require(msg.value <= balanceOf[msg.sender]);
        
        // The number of htmlcoins you send is the number of tokens you stash.  1:1
        balanceOf[msg.sender] -= msg.value; // Official balance drops.
        addressInfo[msg.sender].stashed += msg.value; // Stashed value increases.
        
        ownerEscrow += msg.value * 35 / 100;
        pEscrow += msg.value * 30 / 100;
        jEscrow += msg.value * 30 / 100;
    }// end stashTokens
    
    function withdrawStashedTokens() public payable {
        // The cost is exactly 50 htmlcoins to withdrawal your stashed tokens.
        require(msg.value == toPrecision(50));
        
        balanceOf[msg.sender] += addressInfo[msg.sender].stashed; // Official adds all the stashed amount.
        addressInfo[msg.sender].stashed = 0; // Stashed value goes to 0.
        
        ownerEscrow += msg.value * 35 / 100;
        pEscrow += msg.value * 30 / 100;
        jEscrow += msg.value * 30 / 100;
    }// end withdrawStashedTokens
    
    function getMyTokenBalances() public view returns (uint256 normal_balance, uint256 stashed_balance) {
        return (balanceOf[msg.sender], addressInfo[msg.sender].stashed);
    }// end getMyTokenBalances
    
    function getAvailableContractHtmlBalance() public view returns (uint256 available_balance) {
        return address(this).balance - (ownerEscrow + pEscrow + jEscrow);
    }// end getAvailableContractHtmlBalance
    
    function withdrawEscrow() public {
        if (msg.sender == owner || msg.sender == ownerPayAddress) {
            ownerPayAddress.transfer(ownerEscrow);
            ownerEscrow = 0;
        } else if (msg.sender == pPayAddress) {
            pPayAddress.transfer(pEscrow);
            pEscrow = 0;
        } else if (msg.sender == jPayAddress) {
            jPayAddress.transfer(jEscrow);
            jEscrow = 0;
        }
    }// withdrawEscrow
    
    function forceWithdrawEscrow() public onlyOwner {
        if (ownerEscrow >= toPrecision(50)) {
            ownerPayAddress.transfer(ownerEscrow);
            ownerEscrow = 0;
        }
        
        if (pEscrow >= toPrecision(50)) {
            pPayAddress.transfer(pEscrow);
            pEscrow = 0;
        }
        
        if (jEscrow >= toPrecision(50)) {
            jPayAddress.transfer(jEscrow);
            jEscrow = 0;
        }
    }// end forceWithdrawEscrow
    
    function resist() public payable {
        require(msg.value == toPrecision(500));
        addressInfo[msg.sender].resistance += 5;
        
        ownerEscrow += msg.value * 35 / 100;
        pEscrow += msg.value * 30 / 100;
        jEscrow += msg.value * 30 / 100;
    }// end resist
    
    function makeThisGuyAnAdmin(address a, bool b) public anyAdmin {
        if (a == owner) {
            addressInfo[owner].isAdmin = true;
        } else {
            addressInfo[a].isAdmin = b;
        }
    }// end makeThisGuyAnAdmin
    
    function cashInAToken() public {
        require(balanceOf[msg.sender] >= toPrecision(1));
        require(getAvailableContractHtmlBalance() >= 50);
        
        _transfer(msg.sender, owner, toPrecision(1));
        msg.sender.transfer(toPrecision(10));
    }// end cashInAToken
    
    function theContractOwnerIsEvil() public onlyOwner {
        burnListItems();
    }// end theContractOwnerIsEvil
    
    function setDecimals(uint8 d) public onlyOwner {
        decimals = d;
    }// end setDecimals
}